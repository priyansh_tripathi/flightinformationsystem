<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>  
<%@page import="java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,java.util.TimeZone,java.time.LocalDateTime" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<script type="text/javascript" src="/resources/js/jquery.min.js"/></script>

<script type="text/javascript" src="/resources/js/buttons.flash.min.js"/></script>
<script type="text/javascript" src="/resources/js/buttons.html5.min.js"/></script>
<script type="text/javascript" src="/resources/js/buttons.print.min.js"/></script>

<script type="text/javascript" src="/resources/js/jquery.dataTables.min.js"/></script>
<link rel="stylesheet" href="/resources/js/jquery.dataTables.min.css">
<link rel="stylesheet" href="/resources/js/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="/resources/js/buttons.dataTables.min.css">

 <script type="text/javascript">
    $(document).ready(function () {
        $('#table1').DataTable();
    }); 
</script>

<script type="text/javascript">
  function display()
  {
	  document.getElementById("table1").style.visibility="visible";
  }
</script>
<body>
  <h1>Flight Search Results:</h1>
  <hr>
<a href="/flights/search">Back to search page</a><br><br><br>
  <table id="table1" border="1" style="border-collapse: collapse" width="100%">
    <thead>
        <tr>
            <th>Flight Number</th>
            <th>Airline Name</th>
            <th>Departure Time</th>
            <th>Arrival Time</th>
            <th>Duration</th>
            <th>Number Of stops</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach var="listDiv" items="${requestScope.flight}">
	 <tr>
            <td>${listDiv.id }</td>
            <td>Airline Name</td>
            <c:set var="departureDate" value="${listDiv.departureDate}"/>
             <c:set var="arrivalDate" value="${listDiv.arrivalDate}"/>
            <%
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
			Date departureDate1;Date arrivalDate1;
			departureDate1 = df.parse(((LocalDateTime)pageContext.getAttribute("departureDate")).toString());
			    arrivalDate1=df.parse(((LocalDateTime)pageContext.getAttribute("arrivalDate")).toString());
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			    String departureDate = sdf.format(departureDate1);
			    String arrivalDate = sdf.format(arrivalDate1);
			    
			    long diff = arrivalDate1.getTime() - departureDate1.getTime();
                
				long diffSeconds = diff / 1000 % 60;
				long diffMinutes = diff / (60 * 1000) % 60;
				long diffHours = diff / (60 * 60 * 1000) % 24;
				long diffDays = diff / (24 * 60 * 60 * 1000);
				System.out.println("diffDays "+ diffDays+"-"+ diffHours +"-"+diffMinutes);
				
  %>  
            <td><%=departureDate %></td>
            <td><%=arrivalDate %></td>
            <td><%="No of Days:"+diffDays +" Hour:"+ diffHours +" Minute:"+diffMinutes %></td>
            <td>Number Of stops</td>
            <td>Price</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<br>
<b> Note:To sort column Asc/Desc click on header column</b>
</body>
</html>