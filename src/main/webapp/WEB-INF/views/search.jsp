<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function onSubmitValidate()
{
    var scity=document.getElementById("scity").value;
    var dcity=document.getElementById("desCity").value;
    

    if(scity==dcity)
        {
            alert("Source and Destination should not be same");
            document.getElementById("desCity").focus();
    	    return false; 
        }

      else
        {
    	 // boolean flag= validateData();
    	  //if(validateData())
        	  return true;
    	 
        }
    
}

 /*function validateData()
 {
	    var startDate = document.getElementById("tdate").value;
	    var endDate = document.getElementById("rdate").value;
          return false;
        if((startDate.length>0 && endDate.length>0) || (startDate.length>0 && endDate.length==0))
            {
                 if((startDate.length>0 && endDate.length==0))
                     { 
                        return true;
                      }
                 else if ((Date.parse(startDate) > Date.parse(endDate)))
                 {
	                  alert("End date should be greater than Start date");
       	              return false;
	             }else{
                           return true;
		             }
	     	  
            }
 }  */

</script>

</head>
<body>
<h1>Flight Search:</h1>
  <hr>
  
<form action="flights/searchResult">
<table>
  <tr>
  <td><label for="sourceCity">Source City:(*)</label> </td>
 <td> <select name="scity" id="scity" required="required">
    <option value="DXB">Dubai International Airport(DXB)</option>
    <option value="AUH">Abu Dhabi International Airport(AUH)</option>
    <option value="HEL">Helsinki Vantaa Airport(HEL)</option>
    <option value="GRU">Guarulhos - Governador Andr̩ Franco Montoro International Airport(GRU)</option>
    <option value="HEK">Heihe Airport(HEK)</option>
    <option value="IND">Indianapolis International Airport(IND)</option>
	<option value="COK">Cochin International Airport(COK)</option>
    <option value="GRT">Gujrat Airport(GRT)</option>
    <option value="INJ">Injune Airport(INJ)</option>
	<option value="JAA">Jalalabad Airport(JAA)</option>
    <option value="JAC">Jackson Hole Airport(JAC)</option>
    <option value="IZA">Zona da Mata Regional Airport(IZA)</option>
    <option value="JUR">Jurien Bay Airport(JUR)</option>
	<option value="KCI">Kon Airport(KCI)</option>
</select>
</td>
  <td><label for="destCity">Destination City:(*)</label></td>
  <td>
    <select name="desCity" id="desCity" required="required">
    <option value="DXB">Dubai International Airport(DXB)</option>
    <option value="AUH">Abu Dhabi International Airport(AUH)</option>
    <option value="HEL">Helsinki Vantaa Airport(HEL)</option>
    <option value="GRU">Guarulhos - Governador Andr̩ Franco Montoro International Airport(GRU)</option>
    <option value="HEK">Heihe Airport(HEK)</option>
    <option value="IND">Indianapolis International Airport(IND)</option>
	<option value="COK">Cochin International Airport(COK)</option>
    <option value="GRT">Gujrat Airport(GRT)</option>
    <option value="INJ">Injune Airport(INJ)</option>
	<option value="JAA">Jalalabad Airport(JAA)</option>
    <option value="JAC">Jackson Hole Airport(JAC)</option>
    <option value="IZA">Zona da Mata Regional Airport(IZA)</option>
    <option value="JUR">Jurien Bay Airport(JUR)</option>
	<option value="KCI">Kon Airport(KCI)</option>
</select>
  
  </td>
  </tr>
  
  <tr>
  <td><label for="tdate">Travel Date:(*)</label></td> 
  <td><input type="date" name="tdate" id="tdate" required="required"></td>
  
  <td><label for="rdate">Return Date</label> </td>
    <td><input type="date" name="rdate" id="rdate"></td>
    </tr>
 </table>
 <input type="submit" onclick="return onSubmitValidate();">
</form>
</body>
</html>