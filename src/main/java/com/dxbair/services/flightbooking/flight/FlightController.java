package com.dxbair.services.flightbooking.flight;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.dxbair.services.flightbooking.domain.entity.Flight;

@RestController
@RequestMapping("flights")
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	private static final Logger logger = LoggerFactory.getLogger(FlightController.class);
	
	
	
	  @GetMapping public @ResponseBody List<Flight> getAllFlights() { return
	  flightService.getAllFlights(); }
	 

	
	@GetMapping(value = "/search") 
	public @ResponseBody ModelAndView getAllFlightsSearch() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("search");
		//mv.addObject("flight", flightService.getAllFlights());
		return mv;
	}
	
	 @GetMapping(value = "/flights/searchResult") 
	 public @ResponseBody ModelAndView getAllPassengers(@RequestParam("scity") String scity,@RequestParam("desCity") String desCity,@RequestParam("tdate") String tdate,@RequestParam("rdate") String rdate) throws ParseException 
	  { 
		 logger.info("scity "+scity+  "desCity"+desCity + "tdate"+tdate+ "rdate"+tdate );
		 ModelAndView mv = new ModelAndView();
		  List<Flight> flight = new ArrayList<>();
		  
	  	  flight= flightService.getAllFlights();
	  	  
			List<Flight> filterSearch = flight.parallelStream()
					.filter(f -> f.getDeparture().equals(scity) && f.getArrival().equals(desCity))
					.collect(Collectors.toList());
		  mv.setViewName("allPassengers");
		  mv.addObject("flight",filterSearch);
		  
		  
		  return mv;
	  }
	 
	@GetMapping("/{flight-id}")
	public @ResponseBody Flight getFlightById(@PathVariable("flight-id") String flightId) {
		return flightService.getFlightById(flightId);
	}
	
}
